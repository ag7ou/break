'''
Take a break.
Waits 2 hours then opens a web browser and plays a song.
7/28/18
Part of Udacity intro to Python class
'''

import time
import webbrowser

for i in range(3):
    hours = 2
    minutes = hours * 60
    seconds = minutes * 60
    time.sleep(seconds)

    webbrowser.open('https://www.youtube.com/watch?v=xt0V0_1MS0Q')
